package com.erang.volleytutorial.`object`

import com.erang.volleytutorial.Constants

import org.json.JSONException
import org.json.JSONObject

class Tutorialwing(`object`: JSONObject) {
    var website: String? = null
        private set
    var topics: String? = null
        private set
    var facebook: String? = null
        private set
    var twitter: String? = null
        private set
    var pinterest: String? = null
        private set
    var youtube: String? = null
        private set
    var message: String? = null
        private set

    init {
        parseJsonObject(`object`)
    }

    private fun parseJsonObject(jsonObject: JSONObject) {
        try {
            website = jsonObject.getString(Constants.KEY_WEBSITE)
            topics = jsonObject.getString(Constants.KEY_TOPICS)
            facebook = jsonObject.getString(Constants.KEY_FACEBOOK)
            twitter = jsonObject.getString(Constants.KEY_TWITTER)
            pinterest = jsonObject.getString(Constants.KEY_PINTEREST)
            youtube = jsonObject.getString(Constants.KEY_YOUTUBE)
            message = jsonObject.getString(Constants.KEY_MESSAGE)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }
}