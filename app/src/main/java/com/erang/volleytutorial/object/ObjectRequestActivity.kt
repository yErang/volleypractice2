package com.erang.volleytutorial.`object`

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView

import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.erang.volleytutorial.AppController
import com.erang.volleytutorial.R

import org.json.JSONObject

import java.util.HashMap

class ObjectRequestActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_object_request)

        var view: View? = findViewById(R.id.object_get_request)
        view?.setOnClickListener(this)

        view = findViewById(R.id.object_post_request)
        view?.setOnClickListener(this)
    }

    private fun sendGetRequest() {
        val jsonObjectReq = JsonObjectRequest(JSON_URL, null,
                Response.Listener { response -> showResponse(response, "Showing GET request response...") },
                Response.ErrorListener { })

        // Adding JsonObject request to request queue
        AppController.instance?.addToRequestQueue(jsonObjectReq, REQUEST_TAG)
    }

    private fun sendPostRequest() {

        // Change the url of the post request as per your need...This url is just for demo purposes and
        // actually it does not post data...i.e. it will return same response irrespective of the value you
        // as parameters.

        val jsonObjReq = object : JsonObjectRequest(Request.Method.POST, JSON_URL,
                Response.Listener { response -> showResponse(response, "Showing POST request response...") },
                Response.ErrorListener {
                    //                      VolleyLog.d(TAG, "Error: " + error.getMessage());
                }) {

            // You can send parameters as well with POST request....
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["name"] = "Tutorialwing"
                params["email"] = "tutorialwing@gmail.com"
                params["password"] = "1234567"
                return params
            }
        }

        AppController.instance?.addToRequestQueue(jsonObjReq, POST_REQUEST_TAG)
    }

    private fun sendPostRequestWithHeaders() {

        // Change the url of the post request as per your need...This url is just for demo purposes and
        // actually it does not post data...i.e. it will return same response irrespective of the value you
        // as parameters.

        val jsonObjReq = object : JsonObjectRequest(Request.Method.POST, JSON_URL,
                Response.Listener { response -> showResponse(response, "Showing POST request response...") },
                Response.ErrorListener {
                    //                      VolleyLog.d(TAG, "Error: " + error.getMessage());
                }) {

            // You can send parameters as header with POST request....
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                headers["apiKey"] = "xxxxxxxxxxxxxxx"
                return headers
            }
        }

        AppController.instance?.addToRequestQueue(jsonObjReq, POST_REQUEST_TAG)
    }

    private fun showResponse(response: JSONObject, extra_info: String) {
        val tutorialwing = Tutorialwing(response)
        val text = (extra_info
                + "\n\n Website: " + tutorialwing.website
                + "\n\n Topics: " + tutorialwing.topics
                + "\n\n Facebook: " + tutorialwing.facebook
                + "\n\n Twitter: " + tutorialwing.twitter
                + "\n\n Pinterest: " + tutorialwing.pinterest
                + "\n\n Youtube: " + tutorialwing.youtube
                + "\n\n Message: " + tutorialwing.message)

        val txvResponse = findViewById<View>(R.id.request_response) as TextView
        if (txvResponse != null) {
            txvResponse.text = text
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.object_get_request -> sendGetRequest()
            R.id.object_post_request -> sendPostRequest()
        }
    }

    override fun onStop() {
        super.onStop()
        AppController.instance?.requestQueue?.cancelAll(REQUEST_TAG)
    }

    companion object {

        val REQUEST_TAG = "JSON_OBJECT_REQUEST_TAG"
        val POST_REQUEST_TAG = "JSON_OBJECT_POST_REQUEST_TAG"
        val JSON_URL = "https://tutorialwing.com/api/tutorialwing_details.json"
    }
}
