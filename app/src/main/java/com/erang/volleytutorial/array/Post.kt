package com.erang.volleytutorial.array


import com.erang.volleytutorial.Constants
import org.json.JSONException
import org.json.JSONObject

class Post(jsonObject: JSONObject) {

    var name: String? = null
    var title: String? = null
    var category: String? = null
    var url: String? = null

    init {
        parseJson(jsonObject)
    }

    private fun parseJson(jsonObject: JSONObject) {
        try {
            name = jsonObject.getString(Constants.KEY_NAME)
            title = jsonObject.getString(Constants.KEY_TITLE)
            category = jsonObject.getString(Constants.KEY_CATEGORY)
            url = jsonObject.getString(Constants.KEY_URL)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }
}

