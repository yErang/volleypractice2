package com.erang.volleytutorial.array


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.erang.volleytutorial.R

class ArrayRequestAdapter(posts: Posts, private val rowLayout: Int) : RecyclerView.Adapter<ArrayRequestAdapter.PostViewHolder>() {

    private val posts: List<Post>?

    init {
        this.posts = posts.getPosts()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.name.text = posts!![position].name
        holder.title.text = posts[position].title
        holder.category.text = posts[position].category
        holder.url.text = posts[position].url
    }

    override fun getItemCount(): Int {
        return posts!!.size
    }

    class PostViewHolder(v: View) : RecyclerView.ViewHolder(v) {
           var name:TextView = v.findViewById<View>(R.id.name) as TextView
           var title = v.findViewById<View>(R.id.title) as TextView
           var category = v.findViewById<View>(R.id.category) as TextView
           var url = v.findViewById<View>(R.id.url) as TextView
    }
}
