package com.erang.volleytutorial.array

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View

import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.erang.volleytutorial.AppController
import com.erang.volleytutorial.R
import kotlinx.android.synthetic.main.activity_array_request.*

class ArrayRequestActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_array_request)

        val btnArrayRequest = get_request_array
        btnArrayRequest?.setOnClickListener(this)
    }

    private fun sendRequest() {
        val jsonArrayReq = JsonArrayRequest(JSON_URL,
                Response.Listener { response -> showResponse(Posts(response)) },
                Response.ErrorListener { })

        // Adding JsonObject request to request queue
        AppController.instance?.addToRequestQueue(jsonArrayReq, REQUEST_TAG)
    }

    private fun showResponse(posts: Posts) {
        val recyclerView = request_response_array
        if (recyclerView != null) {
            val postAdapter = ArrayRequestAdapter(posts, R.layout.single_post)
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = postAdapter
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.get_request_array -> sendRequest()
        }
    }

    override fun onStop() {
        super.onStop()
        AppController.instance?.requestQueue?.cancelAll(REQUEST_TAG)
    }

    companion object {

        val REQUEST_TAG = "JSON_ARRAY_REQUEST_TAG"
        val JSON_URL = "https://tutorialwing.com/api/tutorialwing_posts.json"
    }
}

