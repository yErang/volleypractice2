package com.erang.volleytutorial.array

import org.json.JSONException

import org.json.JSONArray


import java.util.ArrayList

class Posts(jsonArray: JSONArray) {
    private var posts: MutableList<Post>? = null

    init {
        parseJson(jsonArray)
    }

    private fun parseJson(jsonArray: JSONArray) {
        posts = ArrayList()
        for (i in 0 until jsonArray.length()) {
            try {
                val jsonObject = jsonArray.getJSONObject(i)
                posts!!.add(Post(jsonObject))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    fun getPosts(): List<Post>? {
        return posts
    }
}

