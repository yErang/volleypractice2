package com.erang.volleytutorial.string

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import android.widget.Toast

import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.erang.volleytutorial.AppController
import com.erang.volleytutorial.R

class StringRequestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_string_request)

        val view = findViewById<View>(R.id.get_request_string)
        view?.setOnClickListener { sendRequest() }
    }

    private fun sendRequest() {
        val stringRequest = StringRequest(JSON_URL,
                Response.Listener { response -> showResponse(response) },
                Response.ErrorListener { error -> Toast.makeText(this@StringRequestActivity, error.message, Toast.LENGTH_LONG).show() })

        AppController.instance?.addToRequestQueue(stringRequest, REQUEST_TAG)
    }

    private fun showResponse(response: String) {
        val txvResponse = findViewById<View>(R.id.request_response_string) as TextView
        if (txvResponse != null) {
            txvResponse.text = response
        }
    }

    companion object {

        val REQUEST_TAG = "STRING_REQUEST_TAG"
        val JSON_URL = "https://tutorialwing.com/api/tutorialwing_welcome.json"
    }

}

