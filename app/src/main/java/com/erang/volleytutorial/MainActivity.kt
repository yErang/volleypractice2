package com.erang.volleytutorial

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.erang.volleytutorial.`object`.ObjectRequestActivity
import com.erang.volleytutorial.array.ArrayRequestActivity
import com.erang.volleytutorial.string.StringRequestActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnStringReq = stringReq
        btnStringReq.setOnClickListener{
            val intentString = Intent(this@MainActivity, StringRequestActivity::class.java)
            startActivity(intentString)
        }

        val btnArrayRequest = arrayReq
        btnArrayRequest.setOnClickListener{
            var intentArray = Intent(this@MainActivity, ArrayRequestActivity::class.java)
            startActivity(intentArray)
        }

        val btnObjectRequest = objectReq
        btnObjectRequest.setOnClickListener{
            var intentObject = Intent(this@MainActivity,ObjectRequestActivity::class.java)
            startActivity(intentObject)
        }
    }
}
