package com.erang.volleytutorial

object Constants {

    var KEY_WEBSITE = "website"
    var KEY_TOPICS = "topics"
    var KEY_FACEBOOK = "facebook"
    var KEY_TWITTER = "twitter"
    var KEY_PINTEREST = "pinterest"
    var KEY_YOUTUBE = "youtube"
    var KEY_MESSAGE = "message"

    var KEY_NAME = "name"
    var KEY_TITLE = "title"
    var KEY_CATEGORY = "category"
    var KEY_URL = "url"
}